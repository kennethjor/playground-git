jQuery ->
	{topics} = window
	ratings = {}
	$ratingButtons = $ "button.rating"
	ratingClasses = ",btn-danger,btn-warning,btn-success,btn-primary".split ","
	# When a button is clicked, make it selected and save the score.
	$ratingButtons.click (event) ->
		$target = $ event.target
		# Import rating values.
		topic = $target.parents("[data-topic]").attr "data-topic"
		aspect = $target.parents("[data-aspect]").attr "data-aspect"
		rating = $target.attr "data-rating"
		ratings[topic] or= {}
		ratings[topic][aspect] = rating
		# Mark only this button as selected.
		$target.parent().find("button").removeClass "active #{ratingClasses.join " "}"
		$target.addClass "active #{ratingClasses[rating]}"
		# Focus next.
		next()
	# Puts the next rating element into focus.
	next = ->
		# Deduce missing elemnts.
		missing = []
		for t, topic of topics
			for a, aspect of topic.aspects
				completed = ratings[t]?[a]?
				unless completed
					missing.push {t, a}
		# If completed, calculate final score.
		if missing.length is 0
			complete()
			missing = false
		else
			# Select first infinished aspect.
			missing = missing[0]
		# Focus correct topic and aspect.
		for t, topic of topics
			$topic = $ "[data-topic='#{t}']"
			$topicFocus = if $topic.is(".panel") then $topic else $topic.find(".panel")
			if missing and t is missing.t
				$topicFocus.addClass "panel-primary"
				$topicFocus.removeClass "panel-info"
			else
				$topicFocus.addClass "panel-info"
				$topicFocus.removeClass "panel-primary"
			for a, aspect of topic.aspects
				$aspect = $topic.find "[data-aspect='#{a}']"
				$aspectFocus = if $aspect.is(".list-group-item") then $aspect else $aspect.find(".list-group-item")
				if missing and t is missing.t and a is missing.a
					$aspect.addClass "list-group-item-info"
				else
					$aspect.removeClass "list-group-item-info"
	# Calculates the final score.
	complete = ->
		$final = $ ".final-score"
		$intro = $final.find ".intro"
		$irrelevant = $final.find ".irrelevant"
		$score = $final.find ".score"
		$scoreText = $final.find ".score-text"
		$final.addClass "panel-primary"
		$final.removeClass "panel-info"
		# Calculate score.
		totalSum = 0
		totalN = 0
		for topic, aspects of ratings
			topicSum = 0
			topicN = 0
			for aspect, rating of aspects
				console.log aspect, rating
				if rating > 0
					topicN++
					topicSum += 1/rating
			if topicN > 0
				totalN++
				totalSum += 1 / (topicN / topicSum)
		$intro.hide()
		if totalN is 0
			$irrelevant.show()
			$score.hide()
		else
			# Finalise score.
			score = totalN / totalSum
			# Normalise score from 1-4 to 1-10
			score = Math.floor score * 3 - 2
			# Show score.
			$irrelevant.hide()
			$score.show()
			$score.find("i.fa").each (i, el) ->
				$el = $ el
				if $el.attr("data-score") <= score
					$el.show()
				else
					$el.hide()
			$scoreText.html "#{score}/10"
	# Focus next.
	next()
	# Debugging.
	#setTimeout (-> $("button").click()), 1000
