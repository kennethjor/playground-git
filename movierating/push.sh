#!/bin/bash
set -e

grunt
(cd build; rsync -qe ssh * kenncom:public_html/movierating/)
