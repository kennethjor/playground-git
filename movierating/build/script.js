(function() {

  jQuery(function() {
    var $ratingButtons, complete, next, ratingClasses, ratings, topics;
    topics = window.topics;
    ratings = {};
    $ratingButtons = $("button.rating");
    ratingClasses = ",btn-danger,btn-warning,btn-success,btn-primary".split(",");
    $ratingButtons.click(function(event) {
      var $target, aspect, rating, topic;
      $target = $(event.target);
      topic = $target.parents("[data-topic]").attr("data-topic");
      aspect = $target.parents("[data-aspect]").attr("data-aspect");
      rating = $target.attr("data-rating");
      ratings[topic] || (ratings[topic] = {});
      ratings[topic][aspect] = rating;
      $target.parent().find("button").removeClass("active " + (ratingClasses.join(" ")));
      $target.addClass("active " + ratingClasses[rating]);
      return next();
    });
    next = function() {
      var $aspect, $aspectFocus, $topic, $topicFocus, a, aspect, completed, missing, t, topic, _ref, _ref1, _results;
      missing = [];
      for (t in topics) {
        topic = topics[t];
        _ref = topic.aspects;
        for (a in _ref) {
          aspect = _ref[a];
          completed = ((_ref1 = ratings[t]) != null ? _ref1[a] : void 0) != null;
          if (!completed) {
            missing.push({
              t: t,
              a: a
            });
          }
        }
      }
      if (missing.length === 0) {
        complete();
        missing = false;
      } else {
        missing = missing[0];
      }
      _results = [];
      for (t in topics) {
        topic = topics[t];
        $topic = $("[data-topic='" + t + "']");
        $topicFocus = $topic.is(".panel") ? $topic : $topic.find(".panel");
        if (missing && t === missing.t) {
          $topicFocus.addClass("panel-primary");
          $topicFocus.removeClass("panel-info");
        } else {
          $topicFocus.addClass("panel-info");
          $topicFocus.removeClass("panel-primary");
        }
        _results.push((function() {
          var _ref2, _results1;
          _ref2 = topic.aspects;
          _results1 = [];
          for (a in _ref2) {
            aspect = _ref2[a];
            $aspect = $topic.find("[data-aspect='" + a + "']");
            $aspectFocus = $aspect.is(".list-group-item") ? $aspect : $aspect.find(".list-group-item");
            if (missing && t === missing.t && a === missing.a) {
              _results1.push($aspect.addClass("list-group-item-info"));
            } else {
              _results1.push($aspect.removeClass("list-group-item-info"));
            }
          }
          return _results1;
        })());
      }
      return _results;
    };
    complete = function() {
      var $final, $intro, $irrelevant, $score, $scoreText, aspect, aspects, rating, score, topic, topicN, topicSum, totalN, totalSum;
      $final = $(".final-score");
      $intro = $final.find(".intro");
      $irrelevant = $final.find(".irrelevant");
      $score = $final.find(".score");
      $scoreText = $final.find(".score-text");
      $final.addClass("panel-primary");
      $final.removeClass("panel-info");
      totalSum = 0;
      totalN = 0;
      for (topic in ratings) {
        aspects = ratings[topic];
        topicSum = 0;
        topicN = 0;
        for (aspect in aspects) {
          rating = aspects[aspect];
          console.log(aspect, rating);
          if (rating > 0) {
            topicN++;
            topicSum += 1 / rating;
          }
        }
        if (topicN > 0) {
          totalN++;
          totalSum += 1 / (topicN / topicSum);
        }
      }
      $intro.hide();
      if (totalN === 0) {
        $irrelevant.show();
        return $score.hide();
      } else {
        score = totalN / totalSum;
        score = Math.floor(score * 3 - 2);
        $irrelevant.hide();
        $score.show();
        $score.find("i.fa").each(function(i, el) {
          var $el;
          $el = $(el);
          if ($el.attr("data-score") <= score) {
            return $el.show();
          } else {
            return $el.hide();
          }
        });
        return $scoreText.html("" + score + "/10");
      }
    };
    return next();
  });

}).call(this);
