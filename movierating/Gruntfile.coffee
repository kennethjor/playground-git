module.exports = (grunt) ->
	# Configure Grunt tasks.
	grunt.initConfig
		coffee:
			script:
				files:
					"build/script.js": "src/script.coffee"

		less:
			style:
				files:
					"build/style.css": "src/style.less"

		jade:
			index:
				options:
					data: -> return topics: require "./build/topics.json"
				files:
					"build/index.html": "src/index.jade"
		
		yaml:
			parts:
				files:
					"build/topics.json": "src/topics.yml"

		watch:
			files: ["src/**"]
			tasks: "default"

	# Load grunt plugins.
	grunt.loadNpmTasks "grunt-contrib-coffee"
	grunt.loadNpmTasks "grunt-contrib-less"
	grunt.loadNpmTasks "grunt-contrib-jade"
	grunt.loadNpmTasks "grunt-yaml"
	grunt.loadNpmTasks "grunt-contrib-watch"

	# Compiles shared stuff.
	grunt.registerTask "default", [
		"yaml"
		"coffee"
		"less"
		"jade"
	]
