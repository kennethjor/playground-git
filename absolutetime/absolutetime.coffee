window.requestAnimationFrame ||= 
	window.webkitRequestAnimationFrame || 
	window.mozRequestAnimationFrame    || 
	window.oRequestAnimationFrame      || 
	window.msRequestAnimationFrame     || 
	(callback, element) ->
		window.setTimeout( ->
			callback(+new Date())
		, 1000 / 60)

$ ->
	$time = $ ".absolutetime-display"

	# // Start with 1 second after the big bang.
	fraction = bigInt(2).pow(64)
	# Calculate UNIX epoc start time.
	epoc = bigInt("1379e7") # 13.79 billion years.
		.multiply("31536000") # seconds in a year.
		.multiply(fraction)

	update = ->
		unix = bigInt(new Date().getTime())
			.multiply(fraction)
			.divide(1000)
		now = unix.add(epoc)
		
		$time.html "#{bigint2hex(now)}"

		requestAnimationFrame update
	
	requestAnimationFrame update

	HEX = "0123456789abcdef".split ""
	bigint2hex = (dec) ->
		hex = ""
		for i in [31..0]
			base = bigInt(16).pow(i)
			remainder = dec.mod(base)
			exponent = dec.subtract(remainder).divide(base)
			hex += HEX[exponent.valueOf()]
			dec = remainder
		return hex
